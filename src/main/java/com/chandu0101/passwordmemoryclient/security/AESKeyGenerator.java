package com.chandu0101.passwordmemoryclient.security;


import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by chandrasekharkode on 12/25/13.
 * It will generate MD5-128 hash for given input string
 */
public class AESKeyGenerator {
    private static final String ALGORITHM = "AES";
    private static final String PASSWORD_HASH_ALGORITHM ="MD5"; // by default JDK support 128 max keylength for AES ,lazy to change provider for  256 bit :(

    public static SecretKey generateKey(String passPhrase) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance(PASSWORD_HASH_ALGORITHM);
        md.update(passPhrase.getBytes("UTF-8"));
        byte[] keyBytes = md.digest();
        return new SecretKeySpec(keyBytes,ALGORITHM);
    }
}
