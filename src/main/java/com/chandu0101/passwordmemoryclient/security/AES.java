package com.chandu0101.passwordmemoryclient.security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import java.util.Base64;

/**
 * Created by chandrasekharkode on 12/22/13.
 */
public class AES {

    private static final String ALGORITHM = "AES";
    private static final String ENCODING = "UTF-8";

    private static final String IV = "KKKKKKKKKKKKKKKK";

    public static String encrypt(String plainText, String encryptionKey)  {
         String result = "";
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, AESKeyGenerator.generateKey(encryptionKey),new IvParameterSpec(IV.getBytes(ENCODING)));
            result = Base64.getEncoder().encodeToString(cipher.doFinal(plainText.getBytes(ENCODING)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String decrypt(String cipherText, String encryptionKey) {
         String result = "";
        try {
            byte[] cipherBytes = Base64.getDecoder().decode(cipherText);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, AESKeyGenerator.generateKey(encryptionKey),new IvParameterSpec(IV.getBytes(ENCODING)));
            result = new String(cipher.doFinal(cipherBytes));

        }catch (Exception e) {
             e.printStackTrace();
        }
        return result;
    }


}
