package com.chandu0101.passwordmemoryclient.util;

/**
 * Created by chandrasekharkode on 12/25/13.
 */
public class StringUtils {

    public static boolean isNotEmpty(String input) {
        boolean result = false;
        if(input!=null && !input.isEmpty()) {
            result = true;
        }
        return result;

    }
}