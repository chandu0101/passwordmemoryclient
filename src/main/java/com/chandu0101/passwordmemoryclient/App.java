package com.chandu0101.passwordmemoryclient;

import com.airhacks.afterburner.injection.InjectionProvider;
import com.chandu0101.passwordmemoryclient.presentation.base.BaseView;
import com.chandu0101.passwordmemoryclient.presentation.login.LoginView;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by chandrasekharkode on 12/22/13.
 */
public class App  extends Application{

    private static App instance;
    private Stage stage;
    private String mastePassword;

    public String getMastePassword() {
        return mastePassword;
    }

    public void setMastePassword(String mastePassword) {
        this.mastePassword = mastePassword;
    }

    public App() {
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }


    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        LoginView view = new LoginView();
        Scene scene = new Scene(view.getView());
        scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());
        stage.setTitle("PaswordMemory Client");
        stage.setScene(scene);
        stage.show();
    }


    @Override
    public void stop() throws Exception {
        InjectionProvider.forgetAll();
    }


    public static void main(String[] args) {
         launch(args);
    }


    public void replaceStageContent(final Parent view) {
       stage.getScene().setRoot(view);
        stage.sizeToScene();
    }

}
