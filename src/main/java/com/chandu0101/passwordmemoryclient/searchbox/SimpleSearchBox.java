package com.chandu0101.passwordmemoryclient.searchbox;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;

/**
 * Created by chandrasekharkode on 12/23/13.
 */
public class SimpleSearchBox<T extends Searchable> extends Region {

    private static final String SPACE = " ";
    private TextField searchField;
    private  FilteredList<T> filteredList;

    public SimpleSearchBox() {
        searchField = new TextField();
        getChildren().add(searchField);
        searchField.textProperty().addListener((ov, oldValue, newValue) ->
                 filteredList.setPredicate(t -> {
                    String[] tokens = newValue.split(SPACE);
                    for(String token : tokens) {
                        if(t.getValue().toLowerCase().contains(token.toLowerCase())) {
                            return true;
                        }
                    }
                    return false;
                }));
    }

    public void setFilteredList(FilteredList<T> filteredList) {
        this.filteredList = filteredList;

    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        searchField.resize(getWidth(),getHeight());
    }
}
