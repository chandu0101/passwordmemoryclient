package com.chandu0101.passwordmemoryclient.presentation.base.addedit;

import com.chandu0101.passwordmemoryclient.entity.BluePrint;
import com.chandu0101.passwordmemoryclient.presentation.base.BasePresenter;
import com.chandu0101.passwordmemoryclient.rest.RestClient;
import com.chandu0101.passwordmemoryclient.security.AES;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Created by chandrasekharkode on 12/25/13.
 */
public class AddEditPresenter implements Initializable {

    @FXML
    private TextField passwordTextField;

    @FXML
    private TextField urlTextField;

    @FXML
    private TextField usernameTextField;

    private BluePrint selectedBluePrint;


    public void setSelectedBluePrint(BluePrint selectedBluePrint) {
        this.selectedBluePrint = selectedBluePrint;
    }

    @FXML
    private Button saveButton;

    private BasePresenter basePresenter;

    public void setBasePresenter(BasePresenter basePresenter) {
        this.basePresenter = basePresenter;
    }

    @FXML
    void handleCancel(ActionEvent event) {
        basePresenter.getAddEditPopOver().hide();
    }

    @FXML
    void handleSave(ActionEvent event) {
        if("Save".equalsIgnoreCase(saveButton.getText())) {
            saveRecord();

        } else if("Update".equalsIgnoreCase(saveButton.getText())) {
           updateRecord();
        }
    }

    private void updateRecord() {
        RestClient restClient = basePresenter.getRestClient();
        selectedBluePrint.setUrl(urlTextField.getText());
        selectedBluePrint.setUsername(usernameTextField.getText());
        selectedBluePrint.setEncryptedPassword(AES.encrypt(passwordTextField.getText(), restClient.getMasterPassword()));
        HashMap<String,String> newvalues = new HashMap<>();
        newvalues.put("url",selectedBluePrint.getUrl());
        newvalues.put("username",selectedBluePrint.getUsername());
        newvalues.put("encryptedPassword",selectedBluePrint.getEncryptedPassword());
        restClient.updatePassword(selectedBluePrint.getId(),newvalues);
        basePresenter.getAddEditPopOver().hide();

    }

    private void saveRecord() {
        BluePrint bluePrint = new BluePrint();
        RestClient restClient = basePresenter.getRestClient();
        bluePrint.setUrl(urlTextField.getText());
        bluePrint.setUsername(usernameTextField.getText());
        bluePrint.setEncryptedPassword(AES.encrypt(passwordTextField.getText(), restClient.getMasterPassword()));
        basePresenter.getAllPasswords().add(restClient.createPassword(bluePrint));
        basePresenter.getAddEditPopOver().hide();
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        BooleanProperty urlEntered = new SimpleBooleanProperty();
        urlEntered.bind(urlTextField.textProperty().isNotEmpty());
        BooleanProperty userNameEntered = new SimpleBooleanProperty();
        userNameEntered.bind(urlTextField.textProperty().isNotEmpty());
        BooleanProperty passwordEntered = new SimpleBooleanProperty();
        passwordEntered.bind(passwordTextField.textProperty().isNotEmpty());
        saveButton.disableProperty().bind(urlEntered.and(userNameEntered).and(passwordEntered).not());
    }

    private void resetUIFields() {
        passwordTextField.setText("");
        urlTextField.setText("");
        usernameTextField.setText("");
    }

    public void showAddScreen() {
        resetUIFields();
        saveButton.setText("Save");
        Point2D point = basePresenter.getAddButton().localToScreen(basePresenter.getAddButton().getLayoutBounds().getMaxX(),basePresenter.getAddButton().getLayoutBounds().getMaxY());
        basePresenter.getAddEditPopOver().show(basePresenter.getAddButton(),point.getX(),point.getY());
    }

    public void showEditScreen() {
        saveButton.setText("Update");
        Point2D point = basePresenter.getEditButton().localToScreen(basePresenter.getEditButton().getLayoutBounds().getMaxX(),basePresenter.getEditButton().getLayoutBounds().getMaxY());
        urlTextField.setText(selectedBluePrint.getUrl());
        usernameTextField.setText(selectedBluePrint.getUsername());
        passwordTextField.setText(selectedBluePrint.getPassword());
        basePresenter.getAddEditPopOver().show(basePresenter.getEditButton(),point.getX(),point.getY());
    }


}
