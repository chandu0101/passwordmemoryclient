package com.chandu0101.passwordmemoryclient.presentation.base;

import com.chandu0101.passwordmemoryclient.entity.BluePrint;
import com.chandu0101.passwordmemoryclient.util.StringUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;

/**
 * Created by chandrasekharkode on 12/25/13.
 */
public class TooltipCell extends TableCell<BluePrint,String> {

    @Override
    protected void updateItem(String s, boolean empty) {
        super.updateItem(s, empty);
        if(!empty && StringUtils.isNotEmpty(s)) {
                BluePrint bluePrint = getTableView().getItems().get(getTableRow().getIndex());
                setText(s);
                Tooltip tooltip = new Tooltip();
                tooltip.setId("tooltip");
                tooltip.textProperty().bind(bluePrint.passwordProperty());
                setTooltip(tooltip);
        } else { // this is needed if table is dynamic otherwise you will see unexpected results
                setText(null);
                setTooltip(null);
        }

    }
}
