package com.chandu0101.passwordmemoryclient.presentation.base;

import com.chandu0101.passwordmemoryclient.App;
import com.chandu0101.passwordmemoryclient.entity.BluePrint;
import com.chandu0101.passwordmemoryclient.presentation.base.addedit.AddEditPresenter;
import com.chandu0101.passwordmemoryclient.presentation.base.addedit.AddEditView;
import com.chandu0101.passwordmemoryclient.presentation.login.LoginPresenter;
import com.chandu0101.passwordmemoryclient.presentation.login.LoginView;
import com.chandu0101.passwordmemoryclient.rest.RestClient;
import com.chandu0101.passwordmemoryclient.searchbox.SimpleSearchBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.controlsfx.control.PopOver;
import org.controlsfx.dialog.Dialogs;

import javax.inject.Inject;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by chandrasekharkode on 12/23/13.
 */
public class BasePresenter implements Initializable {

    @FXML
    private TableView<BluePrint> passwordsTable;

    @FXML
    private Button logoutButton;

    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button editButton;

    @FXML
    private SimpleSearchBox simpleSearchBox;


    private final ObservableList<BluePrint> allPasswords = FXCollections.observableArrayList();
    private FilteredList<BluePrint> filteredPasswords;

    private PopOver addEditPopOver;
    private BasePresenter instance;
    private AddEditPresenter addEditPresenter;

    @Inject
    private RestClient restClient;


    public ObservableList<BluePrint> getAllPasswords() {
        return allPasswords;
    }

    public BasePresenter() {
        instance = this;
    }

    public RestClient getRestClient() {
        return restClient;
    }


    public PopOver getAddEditPopOver() {
        return addEditPopOver;
    }


    public Button getAddButton() {
        return addButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public Button getEditButton() {
        return editButton;
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        restClient.setMasterPassword(App.getInstance().getMastePassword());
        AddEditView addEditView = new AddEditView();
        AddEditPresenter addEditPresenter = (AddEditPresenter) addEditView.getPresenter();
        this.addEditPresenter = addEditPresenter;
        addEditPresenter.setBasePresenter(instance);
        addEditPopOver = new PopOver(addEditView.getView());
        addEditPopOver.setDetachedTitle("Add or Edit Password");
        filteredPasswords = new FilteredList<>(allPasswords);
        prepareTable();
        simpleSearchBox.setFilteredList(filteredPasswords);
        new Thread(() -> {
            List<BluePrint> dataList = restClient.getAllPasswords();
            allPasswords.addAll(dataList);
        }).start();
    }

    private void prepareTable() {
        TableColumn<BluePrint,String> urlColumn = createStringColumn("url","URL");
        TableColumn<BluePrint,String> usernameColumn = createStringColumn("username","Username");
        TableColumn<BluePrint,String> encryptedPasswordColumn = createStringColumn("encryptedPassword","Encrypted Password");
        encryptedPasswordColumn.setCellFactory(tc -> new TooltipCell());
        passwordsTable.getColumns().addAll(urlColumn,usernameColumn,encryptedPasswordColumn);
        passwordsTable.getSelectionModel().setCellSelectionEnabled(true);
        passwordsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        passwordsTable.setItems(filteredPasswords);
    }

    private TableColumn<BluePrint,String> createStringColumn(String name,String title) {
        TableColumn tableColumn = new TableColumn(title);
        tableColumn.setCellValueFactory(new PropertyValueFactory<BluePrint,String>(name));
        return tableColumn;
    }

    private TableColumn<BluePrint,Integer> createIntegerColumn(String name,String title) {
        TableColumn tableColumn = new TableColumn(title);
        tableColumn.setCellValueFactory(new PropertyValueFactory<BluePrint,Integer>(name));
        return tableColumn;
    }

    @FXML
    void handleAddPassword(ActionEvent event) {
        addEditPresenter.showAddScreen();
    }

    @FXML
    void handleDeletePassword(ActionEvent event) {
        BluePrint bluePrint = passwordsTable.getSelectionModel().getSelectedItem();
        if(bluePrint != null) {
           restClient.deletePassword(bluePrint.getId());
           allPasswords.remove(bluePrint);
        } else {
            Dialogs.create()
                    .title("Delete Password")
                    .message("Please Select a row/cell in table before deleting.").showInformation();
        }
    }

    @FXML
    void handleEditPassword(ActionEvent event) {
        BluePrint bluePrint = passwordsTable.getSelectionModel().getSelectedItem();
        if(bluePrint != null) {
          addEditPresenter.setSelectedBluePrint(bluePrint);
          addEditPresenter.showEditScreen();
        } else {
            Dialogs.create()
                    .title("Edit Password")
                   .message("Please Select a row/cell in table before editing.").showInformation();
        }
    }

    @FXML
    void handleLogout(ActionEvent event) {
        LoginView view = new LoginView();
        App.getInstance().replaceStageContent(view.getView());
    }

}
