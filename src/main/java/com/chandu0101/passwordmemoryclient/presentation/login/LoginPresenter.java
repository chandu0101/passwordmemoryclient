package com.chandu0101.passwordmemoryclient.presentation.login;

import com.chandu0101.passwordmemoryclient.App;
import com.chandu0101.passwordmemoryclient.presentation.base.BasePresenter;
import com.chandu0101.passwordmemoryclient.presentation.base.BaseView;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by chandrasekharkode on 12/29/13.
 */
public class LoginPresenter implements Initializable {

    @FXML
    private PasswordField masterPasswordField;
    @FXML
    private Button loginButton;

    @FXML
    void handleLogin(ActionEvent event) {
        App.getInstance().setMastePassword(masterPasswordField.getText());
        BaseView view = new BaseView();
        BasePresenter basePresenter = (BasePresenter) view.getPresenter();
        App.getInstance().replaceStageContent(view.getView());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        BooleanProperty masterPasswordEntered = new SimpleBooleanProperty();
        masterPasswordEntered.bind(masterPasswordField.textProperty().length().greaterThan(3));
        loginButton.disableProperty().bind(masterPasswordEntered.not());
    }


}
