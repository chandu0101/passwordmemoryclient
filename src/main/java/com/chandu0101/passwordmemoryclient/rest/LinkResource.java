package com.chandu0101.passwordmemoryclient.rest;

/**
 * Created by chandrasekharkode on 12/29/13.
 */
public class LinkResource {

    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
