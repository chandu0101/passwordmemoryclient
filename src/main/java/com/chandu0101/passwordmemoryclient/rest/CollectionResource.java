package com.chandu0101.passwordmemoryclient.rest;

import com.chandu0101.passwordmemoryclient.entity.BluePrint;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by chandrasekharkode on 12/24/13.
 */
public class CollectionResource {
    private String href;
    private int offset;
    private int limit;
    private Set<BlueprintResource> items;


    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public Set<BlueprintResource> getItems() {
        return items;
    }

    public void setItems(Set<BlueprintResource> items) {
        this.items = items;
    }
}
