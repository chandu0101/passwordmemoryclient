package com.chandu0101.passwordmemoryclient.rest;

import com.chandu0101.passwordmemoryclient.entity.BluePrint;
import com.chandu0101.passwordmemoryclient.security.AES;
import org.glassfish.jersey.jackson.JacksonFeature;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by chandrasekharkode on 12/23/13.
 */
public class RestClient {

    private static final String BASE_URI ="http://localhost:8080/PasswordMemoryService-1.0/api";
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    private Client client;
    private WebTarget passwordsTarget;
    private String masterPassword;


    @PostConstruct
    public void init() {
       this.client = ClientBuilder.newBuilder().register(JacksonFeature.class).build();
       this.passwordsTarget = client.target(BASE_URI).path("epasswords");
    }

    public List<BluePrint> getAllPasswords() {
        List<BluePrint> bluePrintList = new ArrayList<>();
        Response response = passwordsTarget.request(MediaType.APPLICATION_JSON).get();
        if(response.getStatus()== OK.getStatusCode()) {
            CollectionResource resource = response.readEntity(CollectionResource.class);
            for(BlueprintResource bResource: resource.getItems()) {
                bluePrintList.add(createBluePrint(bResource));
            }
        }
        return bluePrintList;
    }

    public BluePrint getPasswordById(long id) {
       BluePrint bluePrint = null;
       Response response = passwordsTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).get();
       if(response.getStatus() == OK.getStatusCode()) {
           BlueprintResource blueprintResource = response.readEntity(BlueprintResource.class);
           bluePrint = createBluePrint(blueprintResource);
       }
        return bluePrint;
    }

    public BluePrint createPassword(BluePrint bluePrint) {
       BluePrint result = null;
       Response response = passwordsTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(bluePrint));
        if(response.getStatus() == CREATED.getStatusCode()) {
            BlueprintResource blueprintResource = response.readEntity(BlueprintResource.class);
            result = createBluePrint(blueprintResource);
        }
         return result;
    }

    public BluePrint updatePassword(long id,Map newValues) {
        BluePrint result = null;
        Response response = passwordsTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).put(Entity.json(newValues));
        if(response.getStatus() == CREATED.getStatusCode()) {
            BlueprintResource blueprintResource = response.readEntity(BlueprintResource.class);
            result = createBluePrint(blueprintResource);
        }
        return result;
    }

    public String deletePassword(long id) {
        String result = ERROR;
        Response response = passwordsTarget.path(String.valueOf(id)).request(MediaType.APPLICATION_JSON).delete();
        if(response.getStatus() == NO_CONTENT.getStatusCode()) {
            result = SUCCESS;
        }
        return  result;
    }


    private BluePrint createBluePrint(BlueprintResource bResource) {
         BluePrint bPrint = new BluePrint();
         bPrint.setId(Long.valueOf(bResource.getHref().substring(bResource.getHref().lastIndexOf('/')+1)));
         bPrint.setUrl(bResource.getUrl());
         bPrint.setUsername(bResource.getUsername());
         bPrint.setEncryptedPassword(bResource.getEncryptedPassword());
         bPrint.setPassword(AES.decrypt(bResource.getEncryptedPassword(),masterPassword));
         return bPrint;
    }

    public String getMasterPassword() {
        return masterPassword;
    }

    public void setMasterPassword(String masterPassword) {
        this.masterPassword = masterPassword;
    }
}
