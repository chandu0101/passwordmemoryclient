package com.chandu0101.passwordmemoryclient.rest;

/**
 * Created by chandrasekharkode on 12/24/13.
 */
public class BlueprintResource {

    private String href;
    private String url;
    private String username;
    private String encryptedPassword;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlueprintResource)) return false;

        BlueprintResource that = (BlueprintResource) o;

        if (!href.equals(that.href)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return href.hashCode();
    }
}
