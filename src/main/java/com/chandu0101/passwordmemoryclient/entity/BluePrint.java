package com.chandu0101.passwordmemoryclient.entity;

import com.chandu0101.passwordmemoryclient.searchbox.Searchable;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Created by chandrasekharkode on 12/23/13.
 */
public class BluePrint  implements  Searchable{

     @JsonIgnore
    private SimpleLongProperty id;
    private StringProperty url;
    private StringProperty username;
    private StringProperty encryptedPassword;
    @JsonIgnore
    private StringProperty password;

    public BluePrint() {
        this.id= new SimpleLongProperty();
        this.url =  new SimpleStringProperty();
        this.username = new SimpleStringProperty();
        this.encryptedPassword = new SimpleStringProperty();
        this.password = new SimpleStringProperty();
    }

    public long getId() {
        return id.get();
    }

    public SimpleLongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public String getUrl() {
        return url.get();
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public String getUsername() {
        return username.get();
    }

    public StringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getEncryptedPassword() {
        return encryptedPassword.get();
    }

    public StringProperty encryptedPasswordProperty() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword.set(encryptedPassword);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    @Override
    @JsonIgnore
    public String getValue() {
        return getUrl()+"-"+getUsername()+"-"+getEncryptedPassword();
    }
}