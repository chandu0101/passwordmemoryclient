package com.chandu0101.passwordmemoryclient.security;

import static  org.junit.Assert.*;
import org.junit.Test;

/**
 * Created by chandrasekharkode on 12/25/13.
 */
public class AESTest {

    private static final String MASTERKEY = "masterkey";
    private static final String INPUTTEXT = "hello";
    @Test
    public void testEncrypt() throws Exception {
       assertEquals("E48HsFwR8RK2AstuEmC3KQ==" ,AES.encrypt(INPUTTEXT,MASTERKEY));

    }

    @Test
    public void testDecrypt() throws Exception {
        assertEquals(INPUTTEXT,AES.decrypt("E48HsFwR8RK2AstuEmC3KQ==",MASTERKEY));
    }
}
